-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 11 Décembre 2014 à 03:53
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `database`
--

-- --------------------------------------------------------

--
-- Structure de la table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id_doc` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `nom_doc` varchar(50) NOT NULL,
  `auteur_doc` varchar(50) NOT NULL,
  `dateAjout_doc` date NOT NULL,
  PRIMARY KEY (`id_doc`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `documents`
--

INSERT INTO `documents` (`id_doc`, `id`, `nom_doc`, `auteur_doc`, `dateAjout_doc`) VALUES
(1, 0, 'Cahier des charges', 'Gaillard FranÃ§ois', '2014-12-10'),
(2, 0, 'Rapport1', 'Xing', '2014-12-02');

-- --------------------------------------------------------

--
-- Structure de la table `participer`
--

CREATE TABLE IF NOT EXISTS `participer` (
  `id_ut_par` int(20) NOT NULL,
  `id_pro_par` int(20) NOT NULL,
  UNIQUE KEY `id_ut_par` (`id_ut_par`,`id_pro_par`),
  UNIQUE KEY `id_pro_par` (`id_pro_par`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `participer`
--

INSERT INTO `participer` (`id_ut_par`, `id_pro_par`) VALUES
(9, 5);

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE IF NOT EXISTS `projet` (
  `id_pro` int(11) NOT NULL AUTO_INCREMENT,
  `nom_pro` varchar(200) NOT NULL,
  `dateDebut_pro` date NOT NULL,
  `dateFin_pro` date NOT NULL,
  `commanditaire_pro` varchar(50) NOT NULL,
  `libelle_pro` varchar(500) NOT NULL,
  PRIMARY KEY (`id_pro`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `projet`
--

INSERT INTO `projet` (`id_pro`, `nom_pro`, `dateDebut_pro`, `dateFin_pro`, `commanditaire_pro`, `libelle_pro`) VALUES
(1, 'ib', '2014-12-12', '2014-12-26', 'ygcvdt', ''),
(2, 'jhghf', '2014-12-19', '2014-12-25', 'hgtf', ''),
(3, 'qfgqsdfqsdfqsdf', '2014-12-04', '2014-12-26', 'qsdfqsdf', ''),
(4, 'dkjqkldq', '2014-12-11', '2014-12-12', 'dklqjlkqdjk', ''),
(5, 'toto', '2014-12-17', '2014-12-12', 'toto', ''),
(6, 'COALAS', '2014-12-10', '2015-02-14', 'CHU Rouen', ''),
(7, 'VÃ©hicule autonome', '2014-12-09', '2015-07-10', 'Peugeot', ''),
(8, 'Drone automatisÃ©', '2014-09-13', '2015-03-12', 'Siemens', '');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_ut` int(20) NOT NULL AUTO_INCREMENT,
  `nom_ut` varchar(50) NOT NULL,
  `prenom_ut` varchar(50) NOT NULL,
  `profil_ut` int(50) NOT NULL,
  `identifiant_ut` varchar(100) NOT NULL,
  `motDePasse_ut` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ut`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_ut`, `nom_ut`, `prenom_ut`, `profil_ut`, `identifiant_ut`, `motDePasse_ut`) VALUES
(8, 'LARCHE', 'Eric', 1, 'la.eric@irseem.fr', '96f164ad4d9b2b0dacf8ebee2bb1eeb3aa69adf1'),
(9, 'LAOUROU', 'Sophie', 2, 'la.sophie@irseem.fr', '5f50443bfe76f7279a8e0f2f0a98975cdbff38e9'),
(10, 'XING', 'Huibin', 2, 'xi.huibin@irseem.fr', '5b28a5d8a1083cb7fcd520bbd0558ba10910cc0a'),
(11, 'PASCAL', 'Pierre', 2, 'pa.pierre@irseem.fr', 'ff019a5748a52b5641624af88a54a2f0e46a9fb5');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `participer`
--
ALTER TABLE `participer`
  ADD CONSTRAINT `fk_pro` FOREIGN KEY (`id_pro_par`) REFERENCES `projet` (`id_pro`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_utilisateur` FOREIGN KEY (`id_ut_par`) REFERENCES `utilisateur` (`id_ut`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
