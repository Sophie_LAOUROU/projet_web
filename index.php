<?php
include "bdd_connexion.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Page d'accueil</title>
		<link rel="stylesheet" href="style.css">
    </head>
	<body>
	   
		<header>
			<img src="logo.png" alt ="La bannière du site"/>
		</header>
		
		<section id="corps">
			<div id="contenu">
			<p>
				<h1> CONNEXION </h1>
				<p> Veuillez remplir les champs ci-dessous pour accéder à votre page </p>
				<form method="post" action="index.php">
					<label for="identifiant">Identifiant:</label>
					<input type="email" id="identifiant" name="identifiant" required /></br></br>
					<label for="mot_de_passe">Mot de passe:</label> 
					<input type="password" id="mot_de_passe" name="mot_de_passe" required /></br></br></br></br>
					<input class= "bouton" type="submit" value= "Se connecter" name="submit" />
					<input class= "bouton" type="reset" value="Annuler" /> </br></br></br></br>
				</form>
				
				<?php

				if(isset($_POST['submit']))
				{
					$identifiant= htmlspecialchars(trim($_POST['identifiant']));
					$mot_de_passe= htmlspecialchars(trim($_POST['mot_de_passe']));
					if($identifiant AND $mot_de_passe)
					{
						$mot_de_passe = sha1 ($_POST['mot_de_passe']);
						$req = $bdd->prepare("SELECT nom_ut, prenom_ut , id_ut, profil_ut FROM utilisateur WHERE identifiant_ut='$identifiant' AND motDePasse_ut='$mot_de_passe'");
						$req->execute(array('identifiant_ut'=>$identifiant, 'motDePasse_ut' =>$mot_de_passe ));
						
						$rep = $req->fetch();
						if(!$rep) 
						{
						
				?>		
							<p class= "alerte"> "L'identifiant ou le mot de passe saisi est incorrect! </p>
							
							
				<?php			
						}
						else
						{
							session_start();
							$_SESSION['id_ut'] = $rep['id_ut'];
							$_SESSION['identifiant_ut'] = $identifiant; 
							$_SESSION['nom_ut'] = $rep['nom_ut'];    
							$_SESSION['prenom_ut'] = $rep['prenom_ut'];
							
							
							if($rep['profil_ut']== 2)
							{
								echo"Succès";
								header('Location:ma_page.php');
							}
							
							else
								header('Location:ma_page_admin.php');
						}
							
					}

					else echo "Remplir tous les champs";
				}


				?>
			</p>
			</div>
<?php		
include "foot.php";
?>
