
<?php
include "bdd_connexion.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ma page</title>
		<link rel="stylesheet" href="style.css">
    </head>
	<body>
	
		<header>
			<img src="logo.png" alt ="La bannière du site"/>
			<div id="a">
			<p id="utilisateur"> Connecté sous:  <?php 
			if (isset($_SESSION['nom_ut']) AND isset($_SESSION['prenom_ut']))
			echo $_SESSION['nom_ut'] . ' ' . $_SESSION['prenom_ut'];  ?>!</p>
			<a  id= "deco" href="deconnexion.php"> Se déconnecter </a>
			</div>
			<nav>
				<ul>
					<li><a href="ma_page.php">Accueil</a></li>
					<li><a href="liste_chercheurs.php">Liste des chercheurs</a></li>
					<li><a href="ajout_chercheur.php">Ajouter un chercheur</a></li>
					
				</ul>
			</nav>
		</header>
		
		<section id="corps">