<?php
include "session.php";
include "head_admin.php";
?>
		
			<div id="contenu">
				<h1> Bienvenue sur votre page </h1>
				<p> L’IRSEEM, Institut de Recherche en Systèmes Electroniques Embarqués, créé en 2001 par l’ESIGELEC avec l’appui d’industriels et 
				d’institutionnels, a rapidement développé une recherche partenariale et une activité de transfert à destination des filières régionales 
				automobile, aéronautique, électronique, télécommunications et énergie autour d'un thème fédérateur pour ces industries, les systèmes embarqués.
				</p>
			</div>
<?php		
include "foot.php";
?>
